#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <unordered_set>
#include <boost/lexical_cast.hpp>  

void printBoard(std::vector<std::vector<int>> board) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            std::cout << board[i][j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

std::vector<std::vector<int>> emtpyBoard() {
    std::vector<std::vector<int>> empty_board;
    for (int i = 0; i < 8; i++) {
        std::vector<int> row;
        for (int j = 0; j < 8; j++) {
            row.push_back(0);
        }
        empty_board.push_back(row);
    }
    return empty_board;
}

std::vector<std::vector<int>> rotateBoard(std::vector<std::vector<int>> board) {
    std::vector<std::vector<int>> rotated_board = emtpyBoard();
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            rotated_board[j][i] = board[i][7-j];
        }
    }
    return rotated_board;
}

std::vector<std::vector<int>> mirrorBoard(std::vector<std::vector<int>> board) {
    std::vector<std::vector<int>> mirrored_board = emtpyBoard();
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            mirrored_board[i][j] = board[i][7 - j];
        }
    }
    return mirrored_board;
}

void readBoards(std::vector<std::vector<std::vector<int>>>& boards) {
    std::ifstream infile("valid_boards.txt");
    char line;
    for (int i = 0; i < 12; i++) {
        std::vector<std::vector<int>> board = emtpyBoard();
        for (int j = 0; j < 8; j++) {
            for (int k = 0; k < 8; k++) {
                infile >> line;
                int val = line - '0';
                board[j][k] = val;
            }
            /*
            std::stringstream ss(line);
            int val;
            int k = 0;
            while (ss >> val) {
                board[j][k] = val;
                ss.ignore(1);
                k++;
            }*/
        }
        for (int j = 0; j < 4; j++) {
            boards.push_back(board);
            boards.push_back(mirrorBoard(board));
            board = rotateBoard(board);
        }
    }
}

std::unordered_set<std::string> validPos(std::vector<std::vector<std::vector<int>>>& boards) {
    readBoards(boards);
    std::string cols = "abcdefgh";
    std::unordered_set<std::string> valid_positions;
    std::string pos;
    for (int k = 0; k < 92; k++) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (boards[k][i][j] == 1) {
                    std::string s = boost::lexical_cast<std::string>(i + 1);
                    pos = cols[j] + s;
                    valid_positions.insert(pos);
                }
            }
        }
    }
    return valid_positions;
}

void printValid(std::vector<std::vector<std::vector<int>>>& boards, std::string pos) {
    std::string valid_pos;
    std::vector<std::vector<int>> valid_board;
    std::string cols = "abcdefgh";
    bool found_valid = false;
    for (int k = 0; k < 92; k++) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (boards[k][i][j] == 1) {
                    std::string s = boost::lexical_cast<std::string>(i + 1);
                    valid_pos = cols[j] + s;
                    if (valid_pos == pos) {
                        valid_board = boards[k];
                        found_valid = true;
                    }
                }
                if (found_valid) {
                    break;
                }
            }
            if (found_valid) {
                break;
            }
        }
        if (found_valid) {
            break;
        }
    }
    printBoard(valid_board);
}

void checkValid(std::string& pos) {
    std::vector<std::vector<std::vector<int>>> boards;
    std::unordered_set<std::string> valid_positions = validPos(boards);
    auto search = valid_positions.find(pos);
    if (search != valid_positions.end()) {
        std::cout << pos << " is a valid position for a queen in the 8 queen problem" << std::endl;
        std::cout << "\n" << "A solution for this position is this board: \n" << std::endl;
        printValid(boards, pos);
    } else {
        std::cout << pos << " is not a valid postition for a queen in the 8 queen problem";
    }
}

int main() {
    std::string pos;
    std::cout << "Input queen position: ";
    std::cin >> pos;
    checkValid(pos);
    return 0;
}