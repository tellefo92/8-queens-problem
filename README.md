# 8-queens-problem

## Members
- Arne Bjørkelund
- Kai Chen
- Tellef Østereng

## Board layout
The layout of the chess board is starting with a1 in the top left corner of the board, and h8 being the bottom right corner. 
Moving 1 position to the right of a1 moves you to b1, moving 1 position down from b1 moves you to b2 etc.

## Prerequisite

To compile and run the program, g++ version 11 is required. Also, the boost library is required. This can be installed with the following command:
```sh
sudo apt-get install libboost-all-dev
```

## Usage
Compile program with 
```sh
g++ main.cpp -o main
```
and run with
```sh
./main
```
You will be prompted for an input. 
